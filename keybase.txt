==================================================================
https://keybase.io/daviewales
--------------------------------------------------------------------

I hereby claim:

  * I am an admin of https://daviewales.com
  * I am daviewales (https://keybase.io/daviewales) on keybase.
  * I have a public key ASDshfRIcbiehhP1HR9cC8w_uadtXA5vZkE-B2teaIRqYwo

To do so, I am signing this object:

{
  "body": {
    "key": {
      "eldest_kid": "0101af3d5c1b803c783cebdd8b910ffc237960e6a266d4d616a81a328189c2d44b250a",
      "host": "keybase.io",
      "kid": "0120ec85f44871b89e8613f51d1f5c0bcc3fb9a76d5c0e6f66413e076b5e68846a630a",
      "uid": "f56357ef55accfd28088492256441700",
      "username": "daviewales"
    },
    "merkle_root": {
      "ctime": 1615441716,
      "hash": "c6fb272fd5c2eb27a31d3ca158cd4bb86e826b93ed8571d44b3562128a3e216a43cb2640a4a794851eb5f528e8212931088e2ed8119b48029dc4770bf643d8a2",
      "hash_meta": "e078b8e0ee05edd9aaae6cdc94b327df0783150f3ed93808f99a7d69de28f97c",
      "seqno": 19344442
    },
    "service": {
      "entropy": "/tRKTHbLlj3ikS+vmiGX2wkN",
      "hostname": "daviewales.com",
      "protocol": "https:"
    },
    "type": "web_service_binding",
    "version": 2
  },
  "client": {
    "name": "keybase.io go client",
    "version": "5.4.2"
  },
  "ctime": 1615442698,
  "expire_in": 504576000,
  "prev": "f9df5f184fdb00f83b509f6a52ec58231c5b7abd931855f64601ca3d5f895166",
  "seqno": 40,
  "tag": "signature"
}

which yields the signature:

hKRib2R5hqhkZXRhY2hlZMOpaGFzaF90eXBlCqNrZXnEIwEg7IX0SHG4noYT9R0fXAvMP7mnbVwOb2ZBPgdrXmiEamMKp3BheWxvYWTESpcCKMQg+d9fGE/bAPg7UJ9qUuxYIxxber2TGFX2RgHKPV+JUWbEIJAfCRi65RUjvfWHPNy2fGxzZ9Hkmxf0SFmwhBNA5IS4AgHCo3NpZ8RA3Ep3/H+bEhNiWrb19VAVHvS0NMxar6GnbAXA6NWs3b1Ims0ZmGGKlhQXm63Yb+8ramSChuyht4HS7BFjxE6uCqhzaWdfdHlwZSCkaGFzaIKkdHlwZQildmFsdWXEILjSlKwEIDEQE+D9avYYnn4cKimX5N8uDjxcukIhIF/zo3RhZ80CAqd2ZXJzaW9uAQ==

And finally, I am proving ownership of this host by posting or
appending to this document.

View my publicly-auditable identity here: https://keybase.io/daviewales

==================================================================
