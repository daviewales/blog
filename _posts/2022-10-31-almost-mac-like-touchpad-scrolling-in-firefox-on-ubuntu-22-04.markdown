---
layout: post
title: Almost Mac-like touchpad scrolling in Firefox and Thunderbird on Ubuntu 22.04
date: 2022-10-31 15:33:00
author: David Wales
categories:
---

Some time ago, I spent some time trying to [configure scrolling in Firefox on
Ubuntu 20.04]({% post_url 2020-04-26-macbook-pro-ubuntu-20-04-post-install %}).
At the time I discovered [Pavel Fatin's
recommendation](https://pavelfatin.com/scrolling-with-pleasure/#linux) to
launch Firefox with the environment variable `MOZ_USE_XINPUT2=1` set. This was
a good start, but scroll acceleration was still wonky, and kinetic scrolling
was non-existant.

Recently, I updated to Ubuntu 22.04 on Wayland, and discovered a couple of `about:config` tweaks which render the previous workaround redundant,
and result in a much closer approximation of Mac-like scrolling, including both smooth scrolling and kinetic (inertial) scrolling.
The one missing feature is a `hold` gesture to instantly stop the inertial scrolling, but a quick scroll in the opposite direction works almost as well.

There is one caveat: This only works in the traditional `.deb` package version of Firefox.
If you're using snaps, you can have the smooth scrolling, but not the kinetic scrolling. :(

## Show me the configs already!

1. Make sure you're using the `.deb` version of Firefox. (Instructions adapted from [OMGUbuntu](https://www.omgubuntu.co.uk/2022/04/how-to-install-firefox-deb-apt-ubuntu-22-04):

        sudo snap remove firefox
        sudo add-apt-repository ppa:mozillateam/ppa
    
        echo '
        Package: *
        Pin: release o=LP-PPA-mozillateam
        Pin-Priority: 1001
        ' | sudo tee /etc/apt/preferences.d/mozilla-firefox
    
        distro_codename=$(grep UBUNTU_CODENAME /etc/os-release | cut -d = -f 2)
    
        echo 'Unattended-Upgrade::Allowed-Origins:: "LP-PPA-mozillateam:${distro_codename}";' | sudo tee /etc/apt/apt.conf.d/51unattended-upgrades-firefox

2. Go to `about:config` in Firefox and enable the following options:
    - `apz.gtk.kinetic_scroll.enabled=true`
    - `apz.overscroll.enabled=true`
    - _And_... one more setting that I don't recall, because Firefox restarted when I installed the deb version, and I lost all my recent tabs!
      It was something that encouraged Firefox to use the built-in GTK scrolling, rather than reinventing the (scroll)wheel.
      I think it might have been:  
      `apz.gtk.pangesture.enabled=true`

And... I think that's it!
If [three-finger drag gets implemented in libinput](https://gitlab.freedesktop.org/libinput/libinput/-/issues/298), I'll be running out of excuses for staying on Mac!

## Update 2022-11-22:

The following tweaks make it even better:

- `general.smoothScroll.mouseWheel.durationMaxMS=500`
- `general.smoothScroll.mouseWheel.durationMinMS=250`
- `mousewheel.acceleration.start=1`
- `mousewheel.default.delta_multiplier_x=10`
- `mousewheel.default.delta_multiplier_y=10`
- `mousewheel.default.delta_multiplier_z=10`

Thanks to Nitesh Kumar from Linux Hint for the `general.smoothScroll.mouseWheel.duration*` [suggested values](https://linuxhint.com/fix-mouse-wheel-scrolling-sensitivity-in-latest-version-of-firefox/).

## Thunderbird

You can also set most of these tweaks in Thunderbird using the [Config Editor](https://support.mozilla.org/en-US/kb/config-editor).
