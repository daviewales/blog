---
layout: post
title: Ratio of age in the limit
date: 2020-09-03 21:22:00
author: David Wales
categories:
---

Have you ever noticed that for any two people with different ages,
you can always figure out a moment in time when the younger person was exactly half the age of the older person?

Let's say that you are \\(18\\) and your friend is \\(23\\), so there is a \\(5\\) year difference in your ages.
Then, when you were \\(5\\), your friend was \\(10\\), so that was the moment when the ratio of your ages was exactly half.

Does this work for all age differences?

Let the initial age of the younger person be \\(0\\), and the age of the older person be \\(a\\).
Then, at any time \\(t\\) after this, the age of the younger person is \\(0 + t\\)
and the age of the older person is \\(a + t\\).

Hence the ratio of the ages of the two people at any time \\(t\\) is

\\[
  \frac{0 + t}{a + t}.
\\]

Let's look at what happens when we substitute different values for \\(t\\).
We will also let \\(a = 5\\) to say that the second person is \\(5\\) years older than the first.
If we let <math> <mi>t</mi> <mo>=</mo> <mn class="time">0</mn> </math>, then

<div class="mathml-display-outer">
<div class="mathml-display-inner">
<math>
  <mstyle displaystyle="true">
  <mfrac>
    <mrow> <mn>0</mn> <mo>+</mo> <mn class="time">0</mn> </mrow>
    <mrow> <mn>5</mn> <mo>+</mo> <mn class="time">0</mn> </mrow>
  </mfrac>
  <mo>=</mo>
  <mn id="ratio">0</mn>
  </mstyle>
</math>.
</div>
</div>


<div class="mathml-display-outer">
<div class="mathml-display-inner">
<div class="slidecontainer">
  <input type="range" min="0" max="50" value="0" class="slider" id="slider_value">
</div>
</div>
</div>

Note: If you are viewing this in Chrome and you can't see the fraction,
you need to enable [MathML](https://www.w3.org/Math/) by pasting `chrome://flags/#enable-experimental-web-platform-features`
into the address bar, enabling Experimental Web Platform features,
and restarting Chrome.

<div id="canvas-container">
  <canvas id="plot">HTML5 Canvas not supported in this browser</canvas>
</div>

The graph above plots the ratio of the two ages over time. As you move the
slider, you can see that as time approaches infinity, the ratio of the two ages
approaches 1.

(NOTE: The graph is very glitchy if you move the slider quickly, and it doesn't
undo the plot when you slide back to the left. However, I've procrastinated
publishing this post for three years because I haven't been motivated to create
a proper minimal plotting library for it, so I'm just publishing it as-is...)

We can also prove this mathematically using the concept of a limit:

<div class="mathml-display-outer">
<div class="mathml-display-inner">
<math display="block"><mtable columnalign="left"><mtr><mtd><munder><mi>lim</mi><mrow><mi>t</mi><mo>→</mo><mi mathvariant="normal">∞</mi></mrow></munder><mfrac><mi>t</mi><mrow><mi>a</mi><mo>+</mo><mi>t</mi></mrow></mfrac></mtd><mtd><mo>=</mo><munder><mi>lim</mi><mrow><mi>t</mi><mo>→</mo><mi mathvariant="normal">∞</mi></mrow></munder><mfrac><mrow><mi>t</mi><mo>/</mo><mi>t</mi></mrow><mrow><mi>a</mi><mo>/</mo><mi>t</mi><mo>+</mo><mi>t</mi><mo>/</mo><mi>t</mi></mrow></mfrac></mtd></mtr><mtr><mtd></mtd><mtd><mo>=</mo><munder><mi>lim</mi><mrow><mi>t</mi><mo>→</mo><mi mathvariant="normal">∞</mi></mrow></munder><mfrac><mn>1</mn><mrow><mi>a</mi><mo>/</mo><mi>t</mi><mo>+</mo><mn>1</mn></mrow></mfrac></mtd></mtr><mtr><mtd></mtd><mtd><mo>=</mo><munder><mi>lim</mi><mrow><mi>t</mi><mo>→</mo><mi mathvariant="normal">∞</mi></mrow></munder><mfrac><mn>1</mn><mrow><mn>0</mn><mo>+</mo><mn>1</mn></mrow></mfrac></mtd></mtr><mtr><mtd></mtd><mtd><mo>=</mo><mn>1</mn></mtd></mtr></mtable></math>
</div>
</div>

<style>
.mathml-display-inner {
}

.mathml-display-outer {
    width: 100%;
    display: flex;
    justify-content: center;
}

math {
    font-family: KaTeX_Main, "Times New Roman", serif;
    font-size: 1.21em;
}

#canvas-container {
    width: 100%;
    height: 300px;
}

</style>

<script>
  // Fraction values
  let younger = 0
  let older = 5
  let y_max = 1
  //let y_max = 150
  let time_slider = document.getElementById("slider_value")
  let times = document.getElementsByClassName("time")

  function calculateRatio(younger, older, time) {
      return (younger + time) / (older + time)
  }

  function transform_axis(scale, translate) {
      return function(x) {
          return scale * x + translate
      }
  }

  function units_per_pixel(min_unit, max_unit, pixels) {
  // FIXME Not sure if we need this function?
      return (max_unit - min_unit) / pixels
  }

  function f(t) {return calculateRatio(younger, older, t)}

  function points(f, min_x, max_x, n) {
  // Evaluate the function f over n points between min_x and max_x
      scale = (max_x - min_x) / n

      // We generate integer values then scale them rather than generating
      // float values and incrementing by the size of the scale directly in
      // the loop in an attempt to avoid compounding floating point errors.
      // Not sure how necessary this is!
      x_values = []
      for (let i = 0; i < n; i++) {
          x_values[i] = scale * i
      }

      y_values = x_values.map(f)
      return [x_values, y_values]
  }

  function plot(f, a, b, min_x_axis, max_x_axis, canvas) {
  // plot the function f over the interval [a,b]
  // on the axis bounded by min_x_axis and max_x_axis
  // e.g. plot(f, 0, 10, 0, 10) fits a plot of f exactly
  // within the axis boundaries.
  // plot(f, 0, 10, 0, 20) fits a plot of x such that the
  // plot goes from x=0 to x=10, but the canvas goes from
  // x=0 to x=20
  // The function is plotted on the given canvas

  // For maximum smoothness, we want to calculate as many points as
  // there are pixels in canvas.width
  // If we are plotting a domain over a subset of the horizontal axis,
  // we still want to calculate an x value for each pixel


  let y_max = 2 // Replace with actual range

  let x_scale_factor = canvas.width / max_x_axis
  let y_scale_factor = -1 * canvas.height / y_max
  let y_translation = canvas.height
  

  }

  // Canvas live plot
  let canvas = document.getElementById("plot")
  let rect = canvas.parentNode.getBoundingClientRect()
  canvas.width = rect.width
  canvas.height = rect.height
  let context = canvas.getContext("2d")

  let x_scale_factor = canvas.width / +time_slider.max
  let y_scale_factor = -1 * canvas.height / y_max
  let y_translation = canvas.height

  let X = transform_axis(x_scale_factor, 0)
  let Y = transform_axis(y_scale_factor, y_translation)

  //context.save()
  //context.translate(0, canvas.height)
  //context.scale(x_scale_factor, y_scale_factor)

  context.moveTo(X(0), Y(0));

  // Time Slider Callback
  time_slider.oninput = function() {
    for (let i = 0; i < times.length; i++) {
        times[i].innerHTML = time_slider.value;
    }
    ratio = document.getElementById("ratio");
    var ratio_value = calculateRatio(younger, older, +time_slider.value)
    ratio.innerHTML = ratio_value.toFixed(2);

    console.log({
      width: canvas.width,
      height: canvas.height,
      slider: +time_slider.value,
      ratio: +ratio_value,
      x_slider: X(+time_slider.value),
      y_slider: Y(+ratio_value),
      x_scale_factor: x_scale_factor,
      y_scale_factor: y_scale_factor,
    })
    context.lineTo(X(+time_slider.value), Y(+ratio_value))
    context.stroke()
  }

</script>

