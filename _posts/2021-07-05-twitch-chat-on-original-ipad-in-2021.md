---
layout: post
title: Twitch chat on original iPad in 2021
date: 2021-07-05 11:00:00
author: David Wales
categories: [twitch, irc, ipad, legacyjailbreak]
---

**NOTE**: The following is almost certainly the WRONG way to do this. The right way is almost certainly to install [Theos](https://github.com/theos/theos) and create a custom Theos based Makefile to link everything together. (I'll probably try to do this next)

## Aim

To get the Twitch chat to appear on the screen of my brother's [original iPad](https://en.wikipedia.org/wiki/IPad_(1st_generation)).

## Obstacles

iOS 5.1.1 is the latest version of iOS which runs on the original iPad. It was released in 2012.
In 2021, this is what Twitch looks like in Safari on iOS 5.1.1:

![Twitch on original iPad in 2021](/images/twitch-on-original-ipad.png)

Safari just doesn't know what to do with the modern web.

## Background

- Twitch has an [IRC interface](https://dev.twitch.tv/docs/irc/guide).
- You can easily jailbreak the original iPad with the Absinthe jailbreak.
- You can get iOS Terminal from the BigBoss Cydia repo.
- [rirc](https://rcr.io/rirc/) is an open source Terminal IRC client written in C.

## Plan

Could we get a terminal based IRC client running in the iOS Terminal on a jailbroken iPad?
And if so, could we connect it to Twitch to get the Twitch chat?

Spoilers: YES.

## Method

Note: I'm going to assume you know some basic things about a *nix terminal environment,
such as navigating to different directories, running commands, etc.
If not, there are plenty of tutorials waiting for you!

1. Add some essential Cydia repos:
   - [Karen's Repo](https://cydia.akemi.ai/) (Needed to install AppSync Unified)
   - [Sam Bingner's Repo](http://repo.bingner.com/) (Needed for Theos dependencies)
   - BigBoss (Should be pre-installed with Cydia) (Needed for a bunch of things)
2. Install _iOS Terminal_, _iOS Toolchain_ and _Theos Dependencies_ from BigBoss to get the basics installed.
3. Install _AppSync Unified_ from Karen's repo. (Required to run apps which don't have a valid Apple signature)
4. Get a copy of the iOS 5.1 SDK. (I extracted it from a copy of Xcode 4.3.1 which I downloaded from the Apple Developer portal using a free developer account. But there are also copies floating around on other websites.) It's apparently possible to build for the original iPad using newer SDKs, but I wasn't sure, so I went with the 5.1 SDK.
5. Copy the iOS 5.1 SDK to the iPad. I just zipped it and ran a local http server on my laptop with `python3 -m http.server`, then connected the iPad to it and downloaded the SDK. Then in the iOS Terminal I unzipped it with `unzip the-name-of-the-sdk.zip`.
6. Download the rirc source code.
   This is complicated by the fact that GitHub has dropped support for TLS 1.1, and so far as I can tell,
   iOS 5.1.1 doesn't have great support for TLS 1.2, which is the minimum required to talk to GitHub.
   There are a couple of options.
   The secure way is to download the code onto your computer, and then share it to the iPad
   (e.g. using the python server method above.)
   The lazy way is to use an unencrypted connection to GitHub.
   GitHub automatically redirects HTTP to HTTPS, but you can still get an unencrypted connection by swapping to the git protocal.
   So, instead of

       git clone https://github.com/rcr/rirc.git

   you would use

       git clone git://github.com/rcr/rirc.git

   This will be unencrypted, which is bad for security, but should still work perfectly on iOS 5.
7. Patch the Makefiles. Someone really needs to make a tweak for old iOS versions to upgrade them to TLS 1.3.
   But until then, we're going to keep hitting those TLS errors whenever we try to access modern secure sites.
   There is a Makefile in `lib/mbedtls.Makefile` which tries to download a compressed file from GitHub using HTTPS.
   The smart thing to do would be to download the file ourselves and transfer it to the iPad,
   then patch the Makefile to look for the file in a local directory.
   The lazy way is to temporarily host the file on our trusty `python3 -m http.server`,
   and swap out the link in the Makefile. Here's a very minimal diff:

       -MBEDTLS_URL := https://github.com/ARMmbed/mbedtls/archive/v$(MBEDTLS_VER).tar.gz
       +MBEDTLS_URL := http://10.1.1.2:8000/mbedtls-2.25.0.tar.gz
   
   While we're at it, we're going to need to strip out the SHA hash checks, because `shasum` didn't want to work at all.
   Here's another very minimal diff:
   
   ```
   -       @eval $(MBEDTLS_SHA_FILE)
   -       @eval $(MBEDTLS_SHA_CHECK)
   +       #@eval $(MBEDTLS_SHA_FILE)
   +       #@eval $(MBEDTLS_SHA_CHECK)

   -ifneq ($(shell command -v shasum 2>/dev/null),)
   -MBEDTLS_SHA_FILE  := 'echo "$(MBEDTLS_VER_SHA)  $(MBEDTLS_TAR)" > $(MBEDTLS_SHA)'
   -MBEDTLS_SHA_CHECK := 'shasum -c $(MBEDTLS_SHA)'
   -endif
   +#ifneq ($(shell command -v shasum 2>/dev/null),)
   +#MBEDTLS_SHA_FILE  := 'echo "$(MBEDTLS_VER_SHA)  $(MBEDTLS_TAR)" > $(MBEDTLS_SHA)'
   +#MBEDTLS_SHA_CHECK := 'shasum -c $(MBEDTLS_SHA)'
   +#endif
   ```

   Once again, this is terrible for security,
   but if you're motivated you can run these checks on your computer before you send the file to the iPad.
8. Copy the iOS SDK dylibs to where the build system expects to find them.
   Note that I'm using $THEOS paths here, but you should be able to adapt this to wherever path you've saved the SDK:
       
       # switch to super user (root)
       su
       # create a symlink to the dylibs in the SDK.
       # You could probably just copy instead of linking, but linking worked fine.
       ln -s /var/mobile/theos/sdks/iPhoneOS5.1.sdk/usr/lib/system/* /usr/lib/system/
       # switch back to regular user
       exit
9. BUILD THE THING!  
   Assuming you've done all of the above, and I haven't missed any steps,
   you should be able to build `rirc` by simply running the following in the `rirc` directory:

       CC=clang CFLAGS=-I/var/mobile/theos/sdks/iPhoneOS5.1.sdk/usr/include LDFLAGS=-L/var/mobile/theos/sdks/iPhoneOS5.1.sdk/usr/lib/  make
   
   You'll get a bunch of warnings, because we're building with a very outdated version of `clang`, but hopefully no errors.
   After a few seconds, you should end up with a shiny new `./rirc` executable.
10. Fake sign the executable.  
    If you try to run `./rirc` now, you'll get a `killed: 9` error.
    This is because the iPad is unhappy that you're running an unsigned app.
    Assuming you've installed AppSync Unified (see step 1), you just need to fake sign the executable.
    To do this, just run the following:
    ```
    ldid -S rirc
    ```
   
11. Now you should be good to go! Try connecting to Libera Chat (or your favourite IRC server):
    ```
    ./rirc -s irc.libera.chat -p 6697
    ```
    To join a channel, type `/join #channel-name`.
    You can use this to connect to Twitch chat over IRC.
    Just use the `rirc` command from my
    [previous post](https://daviewales.com/twitch/irc/cli/2021/07/05/twitch-chat-in-rirc-irc-client.html)

## Some images:

![Connected to Server](/images/rirc-libera-chat-ipad-1.png)

![Input glitch](/images/rirc-irc-join-channel-ipad-1.png)

![Channel Joined](/images/rirc-irc-channel-joined-ipad-1.png)

