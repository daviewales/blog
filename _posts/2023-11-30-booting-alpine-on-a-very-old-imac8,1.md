---
layout: post
title: Booting Alpine on a Very Old iMac 8,1
date: 2023-11-30 22:47:18+10:00
author: David Wales
categories:
---

I've tried a few times without success to boot Alpine Linux images on very old
Intel Macs. Following the standard instructions, I download the ISO, then image
it directly to a USB flash drive. Using this process, the standard x86_64 image
boots fine on my more recent MacBook Pro 11,2, but crashes the bootloader on
older Macs. (I've also confirmed it crashes the bootloader on a 2010 model
MacBook.)

Recently, I installed Alpine on an original Raspberry Pi, and I noticed that
the instructions were a bit different. Instead of flashing an ISO image, I had
to create a blank FAT 32 disk, and copy the files into, rather than copying
a disk image. I wondered if this process would make any difference to the boot
process on my old iMac. Turns out it does! It booted successfully first try!

## Steps

1. On my Mac, reformat the USB drive as FAT 32, with a Master Boot Record
   partition table. I'm not sure how necessary it is to configure it with
   Master Boot Record, but that's what I did.
2. On a Linux PC, download the Alpine x86_64 standard ISO, and mount it. Then
   copy the files to the USB. NOTE: Copy, from the mounted image, don't flash
   the ISO. (This needs to be done on Linux so we can mount the ISO, as Mac
   won't be able to mount a Linux ISO.)
3. Reboot holding the Option key to enter the bootloader, select the USB disk,
   and boot!
