---
layout: post
title: "Git problems"
date: 2013-05-08 12:46
comments: true
categories: 
---

I was recently messing around in the git repo for this website, and suddenly, I couldn't add a certain folder. I would run `git add <folder>`, but `git commit` and `git status` both said that there was `nothing to commit (working directory clean)`. I tried removing the directory and re-adding it. Didn't work. I tried removing it from git with `git rm`. It said that `'folder/' did not match any files`. Finally, I did `git-ls`, just to see what was actually in the git index. The directory name was there, but none of the enclosed files were listed. Next, I tried `git rm --cache folder/`. Still didn't work. Finally, I tried `git rm --cache folder`. Notice the difference between this and the previous command? All I did was remove the `/` from the end of the folder name. This successfully removed the entry in the git index, and I was able to add the directory again, and everything worked.
