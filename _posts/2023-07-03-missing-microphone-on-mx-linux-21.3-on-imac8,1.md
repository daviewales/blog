---
layout: post
title: Missing Microphone on MX Linux 21.3 on iMac 8,1
date: 2023-07-03 22:17:00+10:00
author: David Wales
categories:
---

This is a quick one. I installed MX Linux 21.3 on an old iMac 8,1 for use as
a general purpose living room PC. It's mainly used for streaming videos and
music. However, sometimes it would be nice to use it for Skype. It's got
a built-in webcam which worked out of the box, but for some reason the
microphone was not detected.

Strangely, I could record audio using `arecord` as root (`sudo su`), but not as
a regular user. (`sudo arecord` didn't work either.) Although the audio
recorded as root was very crackly.

After chasing a bunch of dead-ends, I found the answer on the Arch Linux wiki:
[Microphone not detected by PulseAudio][microphone-pulseaudio]
(This is becoming a bit of a theme for this blog.)

The solution was exactly as described in the link above, but I'll record it
here for posterity (copied from the Arch wiki, loosely edited):

> Determine the card and device number of your mic:
>
> ```
> $ arecord -l
> **** List of CAPTURE Hardware Devices ****
> card 0: Intel [HDA Intel], device 0: ALC889A Analog [ALC889A Analog]
> Subdevices: 1/1
> Subdevice #0: subdevice #0
> card 0: Intel [HDA Intel], device 1: ALC889A Digital [ALC889A Digital]
> Subdevices: 1/1
> Subdevice #0: subdevice #0
> card 0: Intel [HDA Intel], device 2: ALC889A Alt Analog [ALC889A Alt Analog]
> Subdevices: 2/2
> Subdevice #0: subdevice #0
> Subdevice #1: subdevice #1
> ```
> Choose a device and specify it in `hw:CARD,DEVICE` notation. In this case,
> the first device is represented as `hw:0,0`.
>
> Edit `/etc/pulse/default.pa` and insert a `load-module` line specifying your
> device as follows:
>
> ```
> load-module module-alsa-source device=hw:0,0
> # This line should be somewhere before the line
> .ifexists module-udev-detect.so
> ```
> Restart pulseaudio to apply the new settings:
>
> ```
> pulseaudio -k && pulseaudio -D
> ```

After this, the microphone appeared in PulseAudio Volume Control.
I also selected the 'Set as fallback' option in PulseAudio Volumne Control.

After this, `arecord` worked without root, and Skype could find the microphone!
And the recorded audio was crystal clear, which was an improvement over the
crackly audio when recording as root.

[microphone-pulseaudio]: https://wiki.archlinux.org/title/PulseAudio/Troubleshooting#Microphone_not_detected_by_PulseAudio
