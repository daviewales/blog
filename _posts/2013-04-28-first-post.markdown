---
layout: post
title: "First Post!"
date: 2013-04-28 23:27
comments: true
categories: 
---

## Now With Octopress!

I've been hosting a static site on my [Raspberry Pi](http://www.raspberrypi.org) for the last little while as a learning experience, but it hasn't really got a huge amount of use because, well, static sites aren't generally the best blogging platforms. However, that is all about to change. (Actually, it has changed...) In case you have never heard of Octopress, Octopress is a blogging platform. "Like Wordpress?", I hear you ask. No. Not quite. Octopress creates websites that look like dynamic Wordpress type blogs, but are actually cleverly designed static sites. The advantage is that my Raspberry Pi doesn't have to worry about all the load of a dynamic site, but I can still blog. =D I'm going to start migrating my old posts across from [blogspot](http://d-downunder.blogspot.com.au) soon. (Read "I'm going to start migrating my old posts across from blogspot in a future procrastination session.") 

**Edit:** I found a great script to port all my old posts across in a matter of minutes, so here they all are. The site which lead me to the script is [here](http://blog.thepete.net/blog/2012/02/08/blogger-to-octopress/). The actual script can be found [here](https://gist.github.com/baldowl/1578928/download#).

Another really cool aspect of Octopress is that I can type all my blog posts in [Markdown](http://daringfireball.net/projects/markdown/), and it will automatically convert them to HTML.

You will probably notice that this blog is using the default Octopress theme, rather than the fairly amature home made theme I'm using for the rest of the site. I can't quite decide whether to tweak Octopress to look like the rest of the site, or whether to tweak the rest of the site to look like Octopress...

Note that any posts older than this post are posts which have been migrated from my old blogspot blog.

