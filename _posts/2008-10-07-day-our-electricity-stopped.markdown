---
layout: post
title: "The Day Our Electricity Stopped"
date: 2008-10-07T13:40:00-07:00
comments: false
categories:
 - Electricity
 - Poem
 - Alternative Energy
 - Poetry
---

Here's a poem I wrote. (Edit: A long time ago...)

## The Day Our Electricity Stopped

I woke up one morning  
Then jumped out of bed,  
Reached for the light switch,  
Then turned round instead.  

You’re probably wondering  
What prodded my mind,  
When instead of the light switch,  
I reached for the blind.  

It wasn't a thought of electricity saving,  
Nor yet an electricity bill,  
But if you wait quite patiently,  
I’ll tell you the reason. I will.  

The world was shutting down  
Electricity production today.  
The world was out of coal and oil.  
I heard it yesterday.  

We can't use our computer,  
Our email or our phone.  
We can't use laundry timesavers,  
Our washing pile has grown.  

We can't get any news.  
The radio's on strike.  
There isn't even the power  
to power a radio mike.  

For lack of foresight we have paid.  
We've used not wind or water.  
Our solar power isn't made,  
And our spare time is shorter.  


© David Wales, 2008
