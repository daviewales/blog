---
layout: post
title: Cross-compiling unrtf for Android Termux
date: 2023-07-12 23:20:00+10:00
author: David Wales
categories:
---

Recently I received some RTF files via email.
I wanted to read them on my phone, but I didn't have any apps installed which understood RTF.
I thought it would be nice to have a tool in Termux which would dump nicely formatted RTF to the terminal.
At first I thought I might write something to do this myself, but then I discovered [unrtf](https://www.gnu.org/software/unrtf/).

unrtf will read an RTF file and can output plaintext or html, among other formats.
This sounded like exactly what I was looking for.
However, it was not available in the Termux repositories.
Never fear! I'll build it from source I thought.
However, I couldn't install `clang` on in Termux because my phone didn't have enough storage space.

This brought me to cross-compiling, which I haven't done much (any?) of before.
I started to look into cross compiling with clang, which might have worked,
but then I remembered reading an article about how
[zig cc](https://andrewkelley.me/post/zig-cc-powerful-drop-in-replacement-gcc-clang.html) 
could be used for cross-compiling.
I also learnt that `zig` has a built-in musl libc, and supports multiple target architectures out of the box.

It took me a bit of time to figure out the correct invocation to run the build, but it ended up quite simple:

    wget https://ftp.gnu.org/gnu/unrtf/unrtf-0.21.10.tar.gz
    tar -xf unrtf-0.21.10.tar.gz
    cd unrtf-0.21.10
    ./configure CC="zig cc -target arm-linux-musleabi -D_GNU_SOURCE" --host=arm-linux-musleabi
    make

After this, I had a 32 bit arm `unrtf` binary on my computer at `src/unrtf`.
I created an archive containing this and the `outputs/` directory and copied it to my phone.
(The outputs directory is required, and must be specified when running `unrtf` if it is not installed
to the standard location.)
I then ran the executable as follows, which by default outputs HTML to stdout:

    unrtf -P outputs file.rtf

Finally, I piped the output to the [lynx](https://lynx.invisible-island.net/) CLI browser:

    unrtf -P outputs file.rtf | lynx -stdin

This produced very readable output!
