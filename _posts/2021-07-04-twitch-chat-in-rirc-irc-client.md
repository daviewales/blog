---
layout: post
title: Twitch chat in rirc command-line IRC client
date: 2021-07-04 15:00:00
author: David Wales
categories: twitch irc cli
---

Today I got the Twitch chat running using the [rirc](https://github.com/rcr/rirc) command-line IRC client.

Here are the steps so I remember next time I want to do this...

1. Build rirc
   The generic instructions are on the [rirc website](https://rcr.io/rirc/). But I had some mac specific issues which are only solved in the `dev` branch as of this post. So, for me I needed to do the following:
   - Clone the `dev` branch of rirc from [GitHub](https://github.com/rcr/rirc):
     ```
     git clone git@github.com:rcr/rirc.git # The dev branch is currently the default branch
     cd rirc
     ```
   - Build:
     ```
     make
     ```
   - I didn't want to install it to the system, so I didn't run `make install`.
2. Connect to Twitch over IRC using `rirc`
   - Use this link to [get an authentication token](https://twitchapps.com/tmi/). Twitch IRC uses this instead of a password.
   - Assuming you are still in the directory where you built `rirc`, there should be an executable called `rirc`.
     Run the following command, replacing `<authentication_token>` with the token you got in the previous step, and `<username>` with your Twitch username:
     ```
     ./rirc -s irc.chat.twitch.tv -p 6697 -w <authentication_token> -n <username>
     ```
     If it's working, you should see something like this:

     ![Twitch IRC in rirc](/images/rirc-twitch-irc.png)
3. Join your desired channel
   - IRC commands are prepended by a forward slash (/). IRC channels are prepended by a hash (#).
     So for example, to join the channel [bandersaur](https://www.twitch.tv/bandersaur), you would type the following IRC command:
     ```
     /join #bandersaur
     ```
     If it worked, you should see something like:

     ![Join Twitch Channel](/images/rirc-join-twitch-channel.png)

     If it's working you should now be able to send and receive messages!
     
