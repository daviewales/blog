---
layout: post
title: Jailbreak an iPhone 3G running iOS 4.2.1 in 2021
date: 2021-07-11 13:28:00
author: David Wales
categories: [legacyjailbreak]
---

> "And some things that should not have been forgotten were lost. History became legend. Legend became myth. And for two and a half thousand years, the ring passed out of all knowledge." ― Galadriel in The Fellowship of the Ring

Big thanks to Lace Querux from the [LegacyJailbreak community on Discord](https://discord.gg/bhDpTAu) for patiently guiding me through the steps below. Also check out [r/LegacyJailbreak](https://www.reddit.com/r/LegacyJailbreak/) on reddit. 

The biggest challenge for legacy jailbreaking is that many of the old sources no longer exist.
It's also hard to know exactly which version of which tool to use for your device just by searching the internet.
The aim of this post is to document the process in 2021 for an iPhone 3G running 4.2.1,
and to include some hopefully long-lasting Internet Archive links to everything you need.

## Downloads

Note: I've included working Internet Archive links below in an attempt to future-proof this post.
However, you should also be able to get everything you need from the [LegacyJailbreak Mega site](https://mega.nz/folder/k4FAXCIB#Fk7pxs6ikYzL3YBvAGX5ig).

You'll need redsn0w. Version 0.9.6b4 is recommended. Why this particular version?

> I think the future versions have USB communication issues with first gen devices.
> Or at least ones after the redesign might (0.9.9, where it doesn’t ask you for an IPSW whilst giving you more options).
> 0.9.6b4 is just a sure fire version, considering it came out with iOS 4.2.1 support in mind
> — Lace Querux, LegacyJailbreak Discord

Here's an Internet Archive link to an [OS X Daily article about redsn0w 0.9.6b4](https://web.archive.org/web/20101220134740/http://osxdaily.com/2010/11/23/redsn0w-0-9-6b4-download/). And here are the Internet Archive versions of the links within that article to the redsn0w downloads:

- [redsn0w 0.9.6b4 for Mac](https://web.archive.org/web/20110124083230/http://www.iphone-storage.de/redsn0w_mac_0.9.6b4.zip)
- [redsn0w 0.9.6b4 for Windows](https://web.archive.org/web/20110124083228/http://cdn.nspwn.com//redsn0w_win_0.9.6b4.zip)

You'll also need the iOS IPSW file for your specific device and version. At the moment, the best place to find this is the [IPSW Downloads](https://ipsw.me/) site, which provides a friendly user interface, and provides official Apple links to IPSW files.

Alternatively, here is the direct Apple link for the [iPhone 3G iOS 4.2.1 IPSW](http://appldnld.apple.com/iPhone4/061-9853.20101122.Vfgt5/iPhone1,2_4.2.1_8C148_Restore.ipsw).

## Jailbreak

In theory, you should be able to do this from either Mac or Windows.
In practice, I couldn't get it to work on my Mac. This could possibly be because my Mac has USB 3, and I have heard rumours that this causes issues. Instead, I got it working on my wife's Windows laptop using her USB 2 port.

In _theory_, all you need to do now is launch redsn0w and follow the instructions. In practice, I had a bit of trouble getting into DFU mode. (More on that later...)

1. Browse and select the IPSW file.
2. Select that you want to install Cydia. (I chose not to select any of the other options to keep things simple)
3. Follow the instructions in redsn0w to put the device in DFU mode.
4. redsn0w detects the device in DFU mode, jailbreaks it and installs Cydia.

Unfortunately, my power and home buttons are not 100%, and my phone kept either just rebooting, or entering recovery mode. (Recovery mode is when your phone has a picture of the iTunes logo on the screen. DFU is a black screen, but it's still detected by redsn0w and iTunes.)

Thankfully, Lace Querux was able to give me some tips, so here's what worked for me to get the device in DFU mode.

1. First get the device into recovery mode. 
   - Start with the device connected to the computer and turned off.
   - Press the power button to start the device booting.
   - As soon as the device starts booting, release the power button and hold the home button until you see the iTunes logo on the screen.
2. Now that the device is in recovery mode, follow the steps in redsn0w for entering DFU, but **skip the first step**:
   1. Don't do anything for step 1 where is says to just hold down the power button.
   2. Hold down the home and power buttons together for the number of seconds specified in redsn0w (10 seconds)
   3. Keep holding the home button, but release the power button for the number of seconds specified in redsn0w (15 seconds)

At this point, redsn0w should detect that your device is in recovery mode and start the jailbreak!
Just sit back and watch the progress bars.

However, on my Mac I got to this point and it got stuck. redsn0w detected the device in recovery mode, progress bars progressed on screen, but nothing happened on the device, and redsn0w got stuck "waiting for the device to reboot". This is where I switched to my wife's Windows computer and used her USB 2 port.

This time it worked! A pineapple and a progress bar appeared on the iPhone screen and it's jailbreaked!
