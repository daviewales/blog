---
layout: post
title: "Onomatopoeia"
date: 2013-07-17 21:54
comments: true
categories: poems
---

Onomatopoeia  
Is my only fear.  

"Woof" and "moo" and "arrrk"  
Light a fatal spark.  

Sparks can lead to fires.  
Sparks proceed from wires.  

This makes "arrrk" and "moo"  
The spark that travels through.  

Whoosh, the fire's growing.  
Snapple, crack, it's glowing.  

Help if you can hear.  
Onomatopoeia.  

Snarkly drankle arly.  
It moves for its finale.  

Smash! The crackle moo spark  
Slowly dims to dark.  

Mind destruction near.  
Onomatopoeia.  
