---
layout: post
title: "Experimenting with \\(\\KaTeX\\) and \\(\\LaTeX\\)"
date: 2013-05-18 17:11
comments: true
categories: 
---

If this works, I have successfully added ~~MathJax~~ \\(\KaTeX\\) support to my blog. 

$$\frac{4x^2}{\sqrt{2x^\frac{2}{3}}}$$

I followed ~~a mix of~~ the instructions from ~~[here](http://www.idryman.org/blog/2012/03/10/writing-math-equations-on-octopress/#fnref:1) and [here](http://steshaw.org/blog/2012/02/09/hello-mathjax/)~~ [the KaTeX website](https://katex.org/docs/autorender.html).
