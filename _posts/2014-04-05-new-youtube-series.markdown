---
layout: post
title: "New YouTube Series"
date: 2014-04-05 21:30
comments: true
categories: 
---

I have recently started a YouTube series comparing how easy it is to do various tasks on Windows, Mac and Linux.
Because this involves screen recording, I have started the series by comparing screen recording on each OS.

The results are embedded below:

## Mac

<figure class="video container">
  <iframe width="480" height="360" src="https://www.youtube.com/embed/I9OltDchoRo" frameborder="0" allowfullscreen="true"></iframe>
</figure>


## Windows

<figure class="video container">
  <iframe width="480" height="360" src="https://www.youtube.com/embed/cy54w8URQDE" frameborder="0" allowfullscreen="true"></iframe>
</figure>


## Ubuntu Linux

<figure class="video container">
  <iframe width="480" height="360" src="https://www.youtube.com/embed/T6bajeEE7WA" frameborder="0" allowfullscreen="true"></iframe>
</figure>

Also, here are some torrent links, if you are into that sort of thing.
You can use them under the [Creative Commons Non-Commercial license](http://creativecommons.org/licenses/by-nc/4.0/)

These are magnet links, which will work if I am currently seeding.

- [Mac](magnet:?xt=urn:btih:71BB34724485D7F7AF4F571F325C63A60D708791&dn=Screen%20Recording%20Mac.mp4&tr=udp%3a%2f%2ftracker.openbittorrent.com%3a80%2fannounce&ws=http%3a%2f%2fdaviewales.github.io%2fvideos%2fScreen%2520Recording%2fScreen%2520Recording%2520Mac.mp4)
- [Windows](magnet:?xt=urn:btih:B2066CCF32663BA0DBF1EFDC696461B05203B60B&dn=Screen%20Recording%20Windows.mp4&tr=udp%3a%2f%2ftracker.openbittorrent.com%3a80%2fannounce&ws=http%3a%2f%2fdaviewales.github.io%2fvideos%2fScreen%2520Recording%2fScreen%2520Recording%2520Windows.mp4)
- [Ubuntu Linux](magnet:?xt=urn:btih:F0EB8D59881601682EA05F44A31EE5D93BF2D60B&dn=Screen%20Recording%20Ubuntu.mp4&tr=udp%3a%2f%2ftracker.openbittorrent.com%3a80%2fannounce&ws=http%3a%2f%2fdaviewales.github.io%2fvideos%2fScreen%2520Recording%2fScreen%2520Recording%2520Ubuntu.mp4)

If those don't work, you can download torrent files here:
(I am using web seeds, so these will work even if there is no-one in the world seeding.)

- [Mac](http://daviewales.github.io/videos/Screen%20Recording/Screen%20Recording%20Mac.torrent)
- [Windows](http://daviewales.github.io/videos/Screen%20Recording/Screen%20Recording%20Windows.torrent)
- [Ubuntu Linux](http://daviewales.github.io/videos/Screen%20Recording/Screen%20Recording%20Ubuntu.torrent)
