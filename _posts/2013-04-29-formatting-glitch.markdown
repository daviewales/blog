---
layout: post
title: "Formatting glitch."
date: 2013-04-29 11:21
comments: true
categories: 
---

### Mysterious formatting problem

If you scroll down this page, you will notice that once you pass a certain point, all the formatting gets messed up, and the titles are blue and the text is spilling out to the right of where it should be... I don't know why! =(

**EDIT:** I found the problem! When I migrated all my old posts across from blogspot, I used an automatic converter that converted everything into the Octopress format. However, the converter converts everything into Octopress compatible HTML, rather than Markdown. It also dumps the comments in as part of the post. When I deleted the comments from the post, I missed one `</div>` tag, which caused every post after it to get messed up. As you can see now, I've removed the extraneous `</div>` tag, and now the page is rendering perfectly again. =D
