---
layout: post
title: "If you can cook you can program (and vice versa)"
date: 2013-05-07 00:06
comments: true
categories: 
---

Of course, it isn't quite that simple, but the basic ideas remain the same. 

1. Find a good book (or website).
2. Read said resource.
3. Follow instructions in said resource.
4. Stuff up a few times.
5. Learn from your mistakes.
6. Become epic.
7. Impress your friends! Rule the world! (Maybe... =P)

Of course, what I'm really saying is that learning programming (or cooking), especially at the beginning is exactly as easy (or as hard) as following precise instructions. Similarly, becoming a master at programming (or cooking) requires practice, and the instincts learned from a thousand mistakes.

**Disclaimer:** I don't personally cook very much, but I don't think it would be that hard if I wasn't so lazy. This raises another very important point about the parallel's between cooking and programming. You won't learn either if you aren't interested. =P
**Disclaimer 2:** I don't presume to classify myself as an "epic" programmer. I would place myself at about number 5. on the above list. =D
