---
layout: post
title: "How I set up my Pi"
date: 2014-08-01 15:22
comments: true
categories: 
---

I have recently reinstalled Raspbian on my Pi.
The following is a record of the steps I followed to get everything how I like it.
(I'm doing this mainly as a reminder for my future self, but you might find it interesting.)

1. Install Raspbian using the [NOOBS Setup Guide](http://www.raspberrypi.org/help/noobs-setup/).
2. Install [Shairport](https://github.com/abrasive/shairport) using [these instructions](http://www.raywenderlich.com/44918/raspberry-pi-airplay-tutorial).
   (You have to scroll down a long way to get to the good bit! Also note that this site uses an abandoned fork, rather than the original repo.
   The fork still works, but you might consider using the original.)
3. Install [nginx](http://nginx.org) using [Ars Technica's](http://arstechnica.com) [Web Served tutorial series](http://arstechnica.com/gadgets/2012/11/how-to-set-up-a-safe-and-secure-web-server/).
4. Set up the permissions for the website directory using a combination of the top answers from [here](http://serverfault.com/questions/6895/whats-the-best-way-of-handling-permissions-for-apache2s-user-www-data-in-var) and [here](http://serverfault.com/questions/6895/whats-the-best-way-of-handling-permissions-for-apache2s-user-www-data-in-var).
5. Clone personal website (this website) from Bitbucket into the directory prepared above.
6. Harden server using [these instructions](http://plusbryan.com/my-first-5-minutes-on-a-server-or-essential-security-for-linux-servers).
7. Install [BTSync](http://www.bittorrent.com/sync) using [these instructions](http://www.yeasoft.com/site/projects:btsync-deb:btsync-server).
