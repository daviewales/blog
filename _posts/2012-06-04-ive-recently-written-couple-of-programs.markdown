---
layout: post
title: "Some Python Programs"
date: 2012-06-04 00:36
comments: false
---

I’ve recently written a couple of programs using Python. They are both text based programs designed to be run from a command line. The first is designed to help learn about the charges on common ions, and can be downloaded from [here](http://code.google.com/p/learn-ions/). The second is a command line based version of Minesweeper, and can be downloaded from [here](http://code.google.com/p/minesweepercl/). Both programs are very much alpha versions, but they should work. You will need Python 3 to run them, which can be downloaded from [here](http://www.python.org/getit/releases/3.2.3/). NOTE: The Minesweeper game relies on the *nix curses module, and won’t work on Windows.
