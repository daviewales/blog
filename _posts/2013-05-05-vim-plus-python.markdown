---
layout: post
title: "Vim + Python"
date: 2013-05-05 15:13
comments: true
categories: 
---

I'm writing this post, not because there is a dearth of blog posts about getting Vim to play nicely with Python, but because the blog posts I followed referenced outdated versions of the python syntax highlighting scripts. 

The syntax highlighting script I use is [here](http://www.vim.org/scripts/script.php?script_id=790). It has a number of improvements over the older syntax highlighting script I was using. e.g. the older script messed up all formatting after a base class definition like `class NewClass:`, because it expected parentheses before the colon. The new script from the above link has fixed that issue.

I also use the indentation script mentioned [here](http://henry.precheur.org/vim/python.html). The actual script is [here](http://www.vim.org/scripts/script.php?script_id=974)

For gvim / MacVim, I use the colour scheme from [here](http://concisionandconcinnity.blogspot.com.au/2009/07/vim-part-i-improved-python-syntax.html). Unfortunately it doesn't work in the non GUI Vim, so you have to use `colorscheme blackboard` in your `~/.gvimrc`, rather than your `~/.vimrc`.
