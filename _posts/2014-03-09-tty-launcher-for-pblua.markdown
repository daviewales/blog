---
layout: post
title: "TTY Launcher for pbLua"
date: 2014-03-09 01:27
comments: true
categories: 
---

I've been experimenting recently with [pbLua](http://hempeldesigngroup.com/lego/pblua/).
PbLua is custom firmware for Lego Mindstorms NXT robots which allows you to program them
with Lua, rather than the incredibly annoying graphical program they ship with the kits.

However, pbLua is not the most user friendly of firmwares. For instance, once you've
flashed your NXT brick, you connect to it by running the following command:

`screen /dev/tty.usbmodem* 38400`.

This gives you a Lua prompt on your computer which is executed by the NXT.
Essentially, you are turning your computer into an old fashioned dumb terminal,
and the NXT brick is the mainframe... (Ironic right?)

Also, you can't run the above command if it is already running, because the
NXT won't let you connect multiple sessions simultaneously.

This all seemed too confusing to explain to my 8 year old sister, so I decided to wrap
it all up in a nice script. It's not quite foolproof yet, but it's a lot easier than
doing everything manually.

You can [get it from Bitbucket](https://bitbucket.org/dwales/pblua-connect/overview).
Feel free to send me pull requests if you think of any improvements.

It's very easy to use. Just download it and run

    ./nxt

You can stick it in your `$PATH` if you like, but it's not necessary.
