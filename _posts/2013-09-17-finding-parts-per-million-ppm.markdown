---
layout: post
title: "Finding parts per million (ppm)"
date: 2013-09-17 00:44
comments: true
categories: 
---

Everyone gets very confused when they try to learn how to convert from \\(\textrm{g}/\textrm{l}\\) or \\(\textrm{mg}/\textrm{l}\\), or \\(\textrm{mg}/\textrm{g}\\), or something similar, to \\(\textrm{ppm}\\). However, the process is almost identical to finding a percentage. To find a percentage (parts per hundred), you take the part, and the total and do the following:

\\[\textrm{percentage of part in total} = \frac{\textrm{part}}{\textrm{total}}\times 100~\%\\]

To find ppm (parts per million), you take the part, and the total and do the following:

\\[\textrm{parts per million} = \frac{\textrm{part}}{\textrm{total}}\times 1~000~000~\textrm{ppm}\\]

Notice any similarities there?

For example, suppose after a fire we have \\(1~\textrm{g}\\) of carbon in every litre of water in some pond. (I don't know if this is realistic... =P) We first convert everything to the same units. \\(1~\textrm{litre}\\) of water is approximately \\(1000~\textrm{g}\\). Now, we can find the percentage of carbon in the water:

\\[\textrm{percentage} = \frac{1~\textrm{g~C}} {1000~\textrm{g~H$_{\textrm{2}}$O}} \times 100~\% = 0.1~\%\\] 

And the ppm of carbon in the water:

\\[\textrm{ppm} = \frac{1~\textrm{g~C}} {1000~\textrm{g~H$_{\textrm{2}}$O}} \times 1~000~000~\textrm{ppm} = 1000~\textrm{ppm} \\]

Simple right?
