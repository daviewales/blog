---
layout: post
title: "Setting up Emulationstation on a Raspberry Pi"
date: 2013-05-21 12:11
comments: true
categories: RaspberryPi, Emulation
---

I recently installed `emulationstation` using petrockblog's [RetroPie-Setup](https://github.com/petrockblog/RetroPie-Setup) script. However, I had a few issues getting `emulationstation` to work correctly. 

I am using composite video, because I loaned my HDMI cable to friend. This meant that I had a few overscan issues. Initially, I tried changing the overscan using `overscan_left`, `overscan_right`, etc. However, I found that these settings did not affect the overscan for `emulationstation`. Eventually, I discovered I needed to put the line `overscan_scale=1` into my `/boot/config.txt` file.

The next problem I had was that whenever I tried to play any NES roms, the RaspberryPi would crash, and my TV would tell me that nothing was connected. The solution, from this [github issue](https://github.com/Aloshi/EmulationStation/issues/65) is to open `~/.emulationstation/es_systems.cfg` and find the `nes` section. Then find the `COMMAND=` section, and change `/home/pi/RetroPie/supplementary/runcommand/runcommand.sh 1` to `/home/pi/RetroPie/supplementary/runcommand/runcommand.sh 2`

I still have an issue where emulationstation crashes when opening a **second** game. I'll report back if I find a fix.
