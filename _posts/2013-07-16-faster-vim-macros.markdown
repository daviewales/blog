---
layout: post
title: "Faster Vim Macros"
date: 2013-07-16 00:29
comments: true
categories: 
---

Today I needed to run a custom Vim macro 76 times to automatically insert the same (long) entry under a bunch of different Python dictionary keys.
I started the macro recording, typed the first 'template', then ran it with `76@q`. However, it took a very long time. I decided to find out why it took so long. 

Following instructions from [stackoverflow](http://stackoverflow.com/questions/12213597/how-to-see-which-plugins-are-making-vim-slow), I ran a profile while running the macro again. It turned out that the 'GetPythonIndent()' function was the culprit. It took up a whole 260s. (Actually, the '\<SNR\>35\_SearchParensPair()' function inside the 'GetPythonIndent()' function was the real culprit.)

Of course, the best solution would be to find out why those functions were so slow, and write a patch, and submit it. However, I'm lazy, and I don't know Vim Script, so I found another work-around.

The solution (which I think is an improvement over my former macro recording technique, even without the time problem) is to write the repeated code once, then *copy it into a register*. In my case I needed three registers. Then record the macro using pastes from the registers, rather than manually typing. This will prevent Vim needing to use any syntax formatting code, and the macro will run virtually instantaneously.
