---
layout: post
title: Things Which Should Be Obvious in Pandas, But Aren't
date: 2019-08-23 22:00:40
categories: 
---

I have been brushing up on my `pandas` skills lately.
This post records some details which surprised me.

## Printing the first value of a Series **and** its index.

It appears that `pandas` gives different output depending on whether it thinks
you are looking for multiple values from a `Series`, or just one.
But if you want the output style for multiple values applied to just a single
value, you need to trick it by using syntax which would normally only apply
for getting multiple values.

``` python
>>> import pandas as pd
>>> a = pd.Series([1, 2, 3, 4, 6, 7, 9])
>>> print(a[:2])
0    1
1    2
dtype: int64
>>> print(a[0])
1
>>> print(a[:1])
0    1
dtype: int64
>>> a.head(1)
0    1
dtype: int64
```
