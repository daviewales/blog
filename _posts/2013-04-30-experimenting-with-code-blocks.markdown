---
layout: post
title: "Experimenting with code blocks"
date: 2013-04-30 00:17
comments: true
categories: 
---

### Now for some fancy formatting!

Behold! The code block!

#### Fun code block.
~~~ python
#!/usr/bin/env python3

def coolFunction(arg1):
    if arg1 == "cool":
        print("Too right!")
    else:
        print("Really?")

if __name__ == '__main__':
    message = input(': ')
    coolFunction(message)
~~~
