---
layout: post
title: LineageOS Setup
date: 2019-10-18 09:06:00
author: David Wales
categories:
---

This post details the particulars of my LineageOS setup for future reference.

## System

I am running a fairly standard install of [LineageOS](https://www.lineageos.org/).  Specifically, I followed the wiki instructions for the [Motorola Moto G 2014](https://wiki.lineageos.org/devices/titan). I also installed a [minimal set of Google apps](https://wiki.lineageos.org/gapps.html) from [OpenGapps](https://opengapps.org/?api=7.1&variant=nano) as described on the wiki.

In the future, I would probably try [LineageOS for microG](https://lineage.microg.org/) instead, in order to further reduce my dependence on Google services. 

## Apps

There are many benefits to running a Google-free, or Google-lite system. I chose LineageOS in the hope of dramatically decreasing tracking. I also chose it because I wanted to replace the default Google apps with smaller, more focussed open source apps. The main source of tracker-free open source apps is [F-Droid](https://f-droid.org/).

I have slowly worked to transition away from Google apps or Play store apps. The following represents the current state of this transition (in alphabetical order).
Footnotes describe any caveats or issues I have found with the transition app. If there are no footnotes assume the app works well with minimal surprises or issues.

- Calendar → [Simple Calendar](https://f-droid.org/en/packages/com.simplemobiletools.calendar.pro/)[^davx5]
- Camera → Default LineageOS app
- Chrome → [Fennec F-Droid (Firefox for Android\)](https://f-droid.org/en/packages/org.mozilla.fennec_fdroid/)
- Contacts → Default LineageOS app
- Drawing → [Simple Draw Pro](https://f-droid.org/en/packages/com.simplemobiletools.draw.pro/)
- Ebook reader → [Book Reader](https://f-droid.org/en/packages/com.github.axet.bookreader/)[^book-reader]
- Email → [K-9 Mail](https://f-droid.org/en/packages/com.fsck.k9/)
- File Synchronisation → [Syncthing](https://f-droid.org/en/packages/com.nutomic.syncthingandroid/)
- Google Authenticator → [andOTP](https://f-droid.org/en/packages/org.shadowice.flocke.andotp/)
- Maps → [OSMAnd](https://f-droid.org/en/packages/net.osmand.plus/)
- Messages → [Signal](https://signal.org/)[^signal]
- Music → Default LineageOS app
- Notes → [Markor](https://f-droid.org/en/packages/net.gsantner.markor/)
- Office document viewer → [LibreOffice Viewer](https://f-droid.org/en/packages/org.documentfoundation.libreoffice/)[^libreoffice]
- OpenStreetMap editor → [Vespucci](https://f-droid.org/en/packages/de.blau.android/)
- OpenStreetMap quests → [StreetComplete](https://f-droid.org/en/packages/de.westnordost.streetcomplete/)
- PDF viewer → [MuPDF mini](https://f-droid.org/en/packages/com.artifex.mupdf.mini.app/)
- PGP Keychain → [OpenKeychain](https://f-droid.org/en/packages/org.sufficientlysecure.keychain/)
- Password manager → [KeePass DX](https://f-droid.org/en/packages/com.kunzisoft.keepass.libre/)
- Periodic Table → [Elementary](https://f-droid.org/en/packages/com.ultramegatech.ey/)
- Phone → Default LineageOS app
- Play Store → [F-Droid](https://f-droid.org/)[^fdroid]
- Podcasts → [AntennaPod](https://f-droid.org/en/packages/de.danoeh.antennapod/)
- RSS Reader → [Feeder](https://f-droid.org/en/packages/com.nononsenseapps.feeder/<Paste>)[^rss]
- Screen recorder → [ScreenCam](https://f-droid.org/en/packages/com.orpheusdroid.screenrecorder/)
- Steam Guard 2FA → Steam is currently not compatible with TOTP 2FA, so there are no alternatives to the Steam app at the moment.[^steam]
- Task List → [Simpletask](https://f-droid.org/en/packages/nl.mpcjanssen.simpletask/)
- Terminal → [Termux](https://f-droid.org/en/packages/com.termux/)
- TripView → No replacement found so far![^tripview]
- Twilight red screen app → [Red Moon](https://f-droid.org/en/packages/com.jmstudios.redmoon/)
- YouTube app → [NewPipe](https://f-droid.org/en/packages/org.schabi.newpipe/)[^newpipe]
- YouVersion Bible App → [And Bible](https://f-droid.org/en/packages/net.bible.android.activity/)[^bible]

## Benefits


## Footnotes

[^fdroid]: I have currently retained the play store app on my device, as there are a number of apps for which I have not yet found replacements. If I move to a completely Google-free system in the future, I will need to use something like [Yalp](https://github.com/yeriomin/YalpStore) to access Play store apps, or give up Play store apps completely. (Yalp violates the Play store terms of service.)
[^signal]: Signal is not available in F-Droid. To avoid the Play store, I sideload the APK [directly from the Signal website](https://signal.org/android/apk/), and verify the signing certificate by opening it in [ClassyShark3xodus](https://f-droid.org/en/packages/com.oF2pks.classyshark3xodus/) before installing.
[^davx5]: I also use [DAVx5](https://f-droid.org/en/packages/at.bitfire.davdroid/) to connect the calendar app to a CalDAV server.
[^bible]: There are several Bible apps in F-Droid, but they don't have any of the popular modern translations (NIV, ESV, Holman, etc). I plan to switch back to a paper Bible once I finish my current reading plan on YouVersion, with the World English Bible in And Bible as a backup.
[^tripview]: [Transportr](https://f-droid.org/en/packages/de.grobox.liberario/) is the closest alternative to TripView I could find. However, it doesn't appear to work offline, which is a deal-breaker for me. The NSW Government [publishes the timetables here](https://opendata.transport.nsw.gov.au/node/332/exploreapi#!/nswtrains/GetNSWTrains) if you want to fill this gap!
[^book-reader]: There are a number of other good ebook reading options, such as [Librera PRO](https://f-droid.org/en/packages/com.foobnix.pro.pdf.reader/), [FBReader](https://f-droid.org/en/packages/org.geometerplus.zlibrary.ui.android/) and [KOReader](https://f-droid.org/en/packages/org.koreader.launcher/).
[^rss]: There are many many RSS reading options. Just [search RSS](https://search.f-droid.org/?q=rss&lang=en) on F-Droid. I chose Feeder because you don't need to create an account anywhere. It manages all the feeds locally on your device.
[^libreoffice]: To be honest LibreOffice Viewer is quite terrible. It is very slow, and document editing is currently 'experimental'. But it's currently the only option for reading office documents on F-Droid.
[^newpipe]: Watch YouTube in an app without trackers!
[^steam]: [Steam are considering support for third party authenticators, but it is not currently available.](https://support.steampowered.com/kb_article.php?ref=8625-WRAH-9030#nophone)
