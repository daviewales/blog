---
layout: post
title: "Rbenv ruby gem absent after gem install"
date: 2013-09-12 22:29
comments: true
categories: 
---

I thought it would be fun to install [lolcommits](http://mroth.github.io/lolcommits/), which automatically takes a snapshot of you and captions it with your git commit message whenever you do a commit. However, after following the simple installation steps, it appeared that `lolcommits` wasn't actually installed. I tried to enable it in one of my repos, and found that the command was not in my `$PATH`.

Fortunately, I soon found [this site](http://blog.beverlyguillermo.com/post/14813769913/rbenv-and-rubygems-on-a-osx-installation). It reminded me that because I'm using rbenv, I need to run 

    rbenv rehash

after every 

    gem install

That seemed to fix it, and I am about to do my first commit (this post) to see if it works...

### Edit:

Here is the image from that commit...

![image](/images/b6312e9f756.jpg)
