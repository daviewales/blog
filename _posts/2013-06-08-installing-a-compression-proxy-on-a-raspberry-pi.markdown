---
layout: post
title: "Installing a compression proxy on a Raspberry Pi"
date: 2013-06-08 02:20
comments: true
categories: 
---

My friend has a very low data allowance on his internet plan, and mentioned he was using a German compression proxy to compress website data upstream. I thought that it should be possible to set up my own compression proxy on a Raspberry Pi, and after a bit of research discovered [ziproxy](http://ziproxy.sourceforge.net). Installation on Raspbian was simple:

`sudo apt-get install ziproxy`

I then opened the configuration file in `/etc/ziproxy/ziproxy.conf`, and tweaked a few things.

I changed the port to something slightly less obvious.

I enabled `TransparentProxy` (although I'm not sure that I needed to...)

I set the `AuthMode` and `AuthPasswdFile`.

Then I opened the password file, and added some usernames and passwords.

Then I restarted the ziproxy server by running

`/etc/init.d/ziproxy restart`

The next step was to enable port forwarding on my router from the port mentioned above to the internal IP of my Raspberry Pi. This step will be specific to your particular brand of router, so I won't detail it here.

On the client, I was able to set up the proxy just like any other proxy. It worked! Yay!
