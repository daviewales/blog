---
layout: post
title: Troubleshooting Ubuntu 20.04 on a 2014 MacBook Pro 11,2
date: 2020-04-26 20:19:00
author: David Wales
categories:
---

I recently installed Ubuntu 20.04 on my mid 2014 MacBook Pro 11,2.
Most things worked out of the box, but there were a few things which required tweaking.
The following post records these tweaks. The most useful sources for this post were the Arch wiki pages, specifically the [MacBookPro11,x page](https://wiki.archlinux.org/index.php/MacBookPro11,x) and the [general Mac page](https://wiki.archlinux.org/index.php/Mac).

My first impression of Ubuntu 20.04 was that it was extremely slow. It was slow to login, slow to launch applications, slow to scroll in Firefox, slow to register keyboard input.
My second impression was that my laptop was much hotter than usual. After resolving these issues, I realised that the webcam was also non-functional. Additionally, audio playback through my Bluetooth speaker was stuttery. Fortunately, all these issues can be resolved!

## Problem 1: High laptop temperature

Firstly, we need to install a better fan control daemon to get the CPU temperature under control.
There are two options here:

1. [`mbpfan`](https://github.com/linux-on-mac/mbpfan)
2. [`macfanctld`](https://github.com/MikaelStrom/macfanctld)

I installed `mbpfan` which appears to be more actively maintained:

```bash
sudo apt install mbpfan
```

## Problem 2: High CPU usage

Secondly, we need fix the high CPU usage.
According to `top`, the process `kworker` was consistently using more than 50% of one of the CPU cores. `htop` appears to hide kernel processes by default, so it's easier just to use `top` here.
Searching this issue led to a section of the Arch wiki Mac page helpfully titled [kworker using high CPU](https://wiki.archlinux.org/index.php/Mac#kworker_using_high_CPU). According to the wiki, this is 'sometimes the result of runaway ACPI interrupts'. This appears to be generally true for the MacBook Pro 11,2. To check if an ACPI interrupt is causing problems, run the following:

```bash
grep . -r /sys/firmware/acpi/interrupts/
```

Look for an interrupt with hundreds of thousands of interrupts. For a MacBook Pro 11,2, it's probably `gpe06`. (Don't be fooled by `gpe_all`; I'm pretty sure it includes the total number of interrupts from all the numbered `gpexx`, including `gpe06`.)

On my system, I found the following output:

    /sys/firmware/acpi/interrupts/gpe06:38821921

Check that this is the problem by manually disabling it (you might need to use `sudo`):

```bash
echo "disable" > /sys/firmware/acpi/interrupts/gpe06
```

Checking `top`, you should notice that CPU usage has dropped to somewhere close to 2%.
You should also notice that that entire system is rapidly becoming more responsive.

If disabling `gpe06` resolved the issue, we need to make sure that it permanently disabled.
Some have suggested creating a `systemd` script or a `cron` job to run the command above on boot. However, [the official solution](https://bugzilla.kernel.org/show_bug.cgi?id=117481) is to add the following kernel boot parameter to `grub`:

    acpi_mask_gpe=0x06

For example, I edited the file `/etc/default/grub` and added `acpi_mask_gpe=0x06` to the `GRUB_CMDLINE_LINUX_DEFAULT`:

```bash
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash acpi_mask_gpe=0x06"
```

Apparently you can also just add it to `GRUB_CMDLINE_LINUX`.
Don't forget to update `grub` with the new settings by running:

```bash
update-grub
```

## Problem 3. WebCam doesn't work

The MacBook Pro 11,2 has a Broadcom 1570 720p FaceTime HD Camera.
There is no official kernel support for this device, nor are there any official drivers available.
However, there is a third party reverse engineered driver called [bcwc_pcie](https://github.com/patjak/bcwc_pcie). I followed the [installation instructions](https://github.com/patjak/bcwc_pcie/wiki/Get-Started) on the wiki, but initially I encountered a known issue where [`/dev/video` is not created](https://github.com/patjak/bcwc_pcie/issues/49). Strangely, after [following some steps involving `insmod`](https://github.com/patjak/bcwc_pcie/issues/49#issuecomment-277579201) everything worked. I'm not sure whether simply [adding `facetimehd` to `/etc/modules`](https://github.com/patjak/bcwc_pcie/wiki/Get-Started#make-driver-load-correctly-on-startup) and restarting would have worked too.

## Problem 4. Bluetooth audio stutters

I was able to connect to my bluetooth speaker without issue, but audio playback stuttered so much that it was unusable.
After looking at a few possible fixes, the [simplest](https://askubuntu.com/a/1231049) was to install Pulseaudio Volume Control, and change the the latency offset to 45 ms.

## Problem 5. Fix smooth scrolling in Firefox

After fixing all of the above, scrolling in Firefox was still the opposite of smooth. The solution is to force Firefox to use the high-precision touchpad events available in XInput 2.1 as described in Pavel Fatin's blog post [Scrolling with pleasure](https://pavelfatin.com/scrolling-with-pleasure/#linux). You need to ensure that Firefox launches with the environment variable `MOZ_USE_XINPUT2=1` set. For me, the simplest way to do this was to add the following line to `~/.profile`, then logout and login again:

```bash
export MOZ_USE_XINPUT2=1
```

After this, Firefox will scroll like you're using a touchpad, not a scrollwheel with discrete clickpoints! (Still not quite as good as scrolling on Mac, but it's a start!)
