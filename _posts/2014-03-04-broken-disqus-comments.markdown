---
layout: post
title: "Broken Disqus Comments"
date: 2014-03-04 00:09
comments: true
categories: 
---

I have had this [Octopress](http://octopress.org/) blog for some time now, but I've never been able to get [Disqus](http://disqus.com/) comments working. Up until now, that is.

The Octopress instructions suggest that Disqus comments can be easily added by simply creating a Disqus account, and adding the relevant details. However, after adding all my details, all I got was an exciting message which said

> We were unable to load Disqus. If you are a moderator please see our troubleshooting guide.

This problem persisted, despite multiple attempts to fix the problem. I went through all the Disqus settings I could find to no avail. I tried removing the trailing slash from my site name in the Disqus settings. Still nothing. Finally, I tried isolating the problem by installing Disqus on a non-Octopress page. Suddenly, everything worked. Now I had something to work with. I found the Octopress Disqus code inside `source/_includes/disqus.html`, and after some experimentation removed (commented) the following lines:

~~~ javascript
var disqus_identifier = '{{ site.url }}{{ page.url }}';
var disqus_url = '{{ site.url }}{{ page.url }}';
~~~

After running `rake generate`, suddenly Disqus comments started working. I will leave a comment at the end of this post, just to prove it.

I have no idea why this works, or why it didn't work before...
